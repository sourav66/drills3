function vitamin()
{
    const items = [{
        name: 'Orange',
        available: true,
        contains: "Vitamin C",
    }, {
        name: 'Mango',
        available: true,
        contains: "Vitamin K, Vitamin C",
    }, {
        name: 'Pineapple',
        available: true,
        contains: "Vitamin A",
    }, {
        name: 'Raspberry',
        available: false,
        contains: "Vitamin B, Vitamin A",
    
    }, {
        name: 'Grapes',
        contains: "Vitamin D",
        available: false,
    }];

    // 1.
    const result1 = items.filter(value => value.available === true);

    const result2 = items.map(value =>
        {
            let array = value.contains;
            array = array.split(',');
            if(array.length === 1)
            {
                if(array[0] === "Vitamin C")
                    return value;
            }
        }
    );
    
    // const result2 = items.filter(value => ((value.contains).split(',')).length === 1 )


    console.log(result1);
    console.log(result2);
}
vitamin();